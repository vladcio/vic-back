const express = require('express');
const router = express.Router();
const Picture = require('../models/pictures');
const multer = require('multer');
const upload = multer({ dest: './public/uploads/' });

/* GET pictures listing. */
router.get('/pictures', (req, res, next) => {
  Picture.find({})
    .then((pictures) => res.json(pictures))
    .catch(next);
});


router.post('/find-category', (req, res, next) => {
  const categoryType = req.body.categoryType;
  Picture.find({categoryType: {$regex: categoryType, $options: 'i'}})
    .then((pictures) => res.json(pictures))
    .catch(next)
});

router.post('/new-picture', upload.single('uploadedFile'), (req, res, next) => {
  const name = req.body.name;
  const categoryType = req.body.categoryType;
  const picPath = `/uploads/${req.body.picPath}`;
  const description = req.body.description;
  const idReference = req.body.idReference;
  const modelType = req.body.modelType;

  if (!req.user) {
    res.redirect('/auth/login');
  }

  Picture.findOne({ idReference }, '_id', (err, fountItem) => {
    if (fountItem) {
      res.status(400).json({ error: 'The item already exists' });
      return;
    }

    const newPicture = new Picture({
      name,
      categoryType,
      picPath,
      description,
      idReference,
      modelType,
    });
  
    newPicture.save((err) => {
      if (err) {
        res.status(400).json({ error: 'Something went wrong' });
        return;
      }
    });
    if (err) {
      res.status(500).json({ error: 'Something went wrong' });
    }
  });


});

module.exports = router;