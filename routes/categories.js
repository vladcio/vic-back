const express = require('express');
const router = express.Router();
const Item = require('../models/categories');
const multer = require('multer');
const upload = multer({ dest: './public/uploads/' });

/* GET items listing. */
router.get('/categories-items', (req, res, next) => {
  Item.find({})
    .then((items) => res.json(items))
    .catch(next);
});


router.post('/findcat', (req, res, next) => {
  const categoryType = req.body.categoryType;
  Item.find({categoryType: {$regex: categoryType, $options: 'i'}})
    .then((items) => res.json(items))
    .catch(next)
});

router.post('/item-photo', upload.single('uploadedFile'), (req, res, next) => {
  if (!req.user) {
    return res.status(401).json({error: 'unauthorized'});
  }
  res.send(res.req.file);
});

router.post('/new-item', upload.single('uploadedFile'), (req, res, next) => {
  if (!req.user) {
    res.redirect('/auth/login');
  }
  const name = req.body.name;
  const categoryType = req.body.categoryType;
  const picPath = `/uploads/${req.body.picPath}`;
  const description = req.body.description;
  const idReference = req.body.idReference;

  
  Item.findOne({ idReference }, '_id', (err, fountItem) => {
    if (fountItem) {
      res.status(400).json({ error: 'The item already exists' });
      return;
    }
    
    const newItem = new Item({
      name,
      categoryType,
      picPath,
      description,
      idReference,
    });
    
    newItem.save((err) => {
      if (err) {
        res.status(400).json({ error: 'Something went wrong' });
        return;
      }
    });
    if (err) {
      res.status(500).json({ error: 'Something went wrong' });
    }
  });

});

module.exports = router;