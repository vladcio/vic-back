const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();
const passport = require('passport');
const ensureLogin = require('connect-ensure-login');
const nodemailer = require('nodemailer');
const emailPass = process.env.EMAIL_TOKEN;
const emailUser = process.env.EMAIL_USER;
const emailHost = process.env.EMAIL_HOST;
const emailPort = process.env.EMAIL_PORT;
const emailFrom = process.env.EMAIL_FROM;
const emailSubject = process.env.EMAIL_SUBJECT;
const emailTo = process.env.EMAIL_TO;

const transporter = nodemailer.createTransport({
  host: emailHost,
  port: emailPort,
  auth: {
    user: emailUser, // your domain email address
    pass: emailPass, // your password
    tls:{
      rejectUnauthorized:false
    }
  }
});

const User = require('../models/user');

// check which user is logged in

router.get('/me', (req, res, next) => {
  if (req.user) {
    res.json(req.user);
  } else {
    res.status(404).json({error: 'not-found'});
  }
});
router.post('/mark-as-read-message', (req, res, next) => {
  const userId = req.body.userId;
  const messageId = req.body.messageId

  User.findById(userId)
      .then(user => {
    let receivedMessage = user.receivedMessage.id(messageId);
    if (receivedMessage.unreaded === true) {
      receivedMessage.unreaded = false;
    } else {
      receivedMessage.unreaded = true;
    }
    return user.save();
  })
  .then(() => {
    User.findById(userId)
      .then(user => {
      res.json(user);
    })
  })
});

router.post('/mark-as-read', (req, res, next) => {
  const userId = req.body.userId;
  const messageId = req.body.messageId

  User.findById(userId)
      .then(user => {
    let receivedOffer = user.receivedOffer.id(messageId);
    if (receivedOffer.unreaded === true) {
      receivedOffer.unreaded = false;
    } else {
      receivedOffer.unreaded = true;
    }
    return user.save();
  })
  .then(() => {
    User.findById(userId)
      .then(user => {
      res.json(user);
    })
  })
});

router.post('/edit-offer', (req, res, next) => {
  const userId = req.body.userId;
  const messageId = req.body.messageId;
  const offerEdited = req.body.offerEdited;
  const date = new Date();
  const dateFormated = date.toLocaleString('en-ES');
  if (offerEdited.profession) {
    this.profession = offerEdited.profession 
  } else {
    this.profession = offerEdited.otherProfession
  }

  if (offerEdited.application) {
    this.application = offerEdited.application 
  } else {
    this.application = offerEdited.otherApplication
  }
  
  User.findById(userId)
  .then(user => {
    this.user = user;
    let receivedOffer = user.receivedOffer.id(messageId);
    receivedOffer.firstName = offerEdited.firstName,
    receivedOffer.surName = offerEdited.surName,
    receivedOffer.phone = offerEdited.phone,
    receivedOffer.email = offerEdited.email,
    receivedOffer.companyName = offerEdited.companyName,
    receivedOffer.address = offerEdited.address,
    receivedOffer.zipCode = offerEdited.zipCode,
    receivedOffer.city = offerEdited.city,
    receivedOffer.country = offerEdited.country,
    receivedOffer.profession = offerEdited.profession,
    receivedOffer.otherProfession = offerEdited.otherProfession,
    receivedOffer.application = offerEdited.application,
    receivedOffer.otherApplication = offerEdited.otherApplication,
    receivedOffer.categoryType = offerEdited.categoryType,
    receivedOffer.modelId = offerEdited.modelId,
    receivedOffer.width = offerEdited.width,
    receivedOffer.height = offerEdited.height,
    receivedOffer.projectName = offerEdited.projectName,
    receivedOffer.messageContent = offerEdited.messageContent,
    receivedOffer.requestStatus = offerEdited.requestStatus,
    receivedOffer.internalComments = offerEdited.internalComments
    return user.save();
  })
  .then(() => {
    if (offerEdited.internalComments === offerEdited.initialComment) {
      const mailOptions = {
        from: emailFrom,
        to: offerEdited.email,
        bcc: emailTo,
        subject: emailSubject + offerEdited.offerId.toString() + ' ' + offerEdited.firstName + ' ' + offerEdited.surName + ' ' + offerEdited.projectName,
        html:  
        'According to the previous conversation your offer request number ' + offerEdited.offerId.toString() + ' was updated with following informations: ' + '<br>' +
        '<br>' +
        'First Name: ' + offerEdited.firstName + '<br>' +
        'Surname: ' + offerEdited.surName + '<br>' +
        'Email: ' + offerEdited.email + '<br>' +
        'Phone: ' + offerEdited.phone + '<br>' +
        'Company Name: ' + offerEdited.companyName + '<br>' +
        'Address: ' + offerEdited.address + '<br>' +
        'City: ' + offerEdited.city + '<br>' +
        'ZipCode: ' + offerEdited.zipCode + '<br>' +
        'Country: ' + offerEdited.country + '<br>' +
        'Profession: ' + this.profession + '<br>' +
        'Application: ' + this.application + '<br>' +
        'Category Type: ' + offerEdited.categoryType + '<br>' +
        'Model Id: ' + offerEdited.modelId + '<br>' +
        'Width: ' + offerEdited.width + ' cm' + '<br>' +
        'Height: ' + offerEdited.height + ' cm' + '<br>' +
        'Project Name: ' + offerEdited.projectName + '<br>' +
        'Message Content: ' + offerEdited.messageContent + '<br>' +
        'Processing Status: ' + offerEdited.requestStatus + 
        '<br>' +
        'Modified At: ' + dateFormated,
      };
      return transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      })
    }
   })
  .then(() => {
    User.findById(userId)
    .then(user => {
      res.json(user);
    })
  })
});

router.post('/edit-message', (req, res, next) => {
  const userId = req.body.userId;
  const messageId = req.body.messageId;
  const messageEdited = req.body.messageEdited;
  
  User.findById(userId)
  .then(user => {
    let receivedMessage = user.receivedMessage.id(messageId);
    receivedMessage.name = messageEdited.name,
    receivedMessage.phone = messageEdited.phone,
    receivedMessage.email = messageEdited.email,
    receivedMessage.message = messageEdited.messageContent,
    receivedMessage.requestStatus = messageEdited.requestStatus
    return user.save();
  })
  .then(() => {
    User.findById(userId)
    .then(user => {
      res.json(user);
    })
  })
});

// login local user

router.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, theUser, failureDetails) => {
    const username = req.body.username;
    const password = req.body.password;
    if (err) {
      res.status(500).json({ message: 'Something went wrong' });
      return;
    }

    if (req.user) {
      return res.status(401).json({error: 'Unauthorized'});
    }
    if (!username || !password) {
      return res.status(422).json({error: 'Insert username and password'});
    }

    if (!theUser) {
      res.status(401).json(failureDetails);
      return;
    }

    req.login(theUser, (err) => {
      if (err) {
        res.status(500).json({ message: 'Something went wrong' });
        return;
      }

      res.status(200).json(req.user);
    });
  })(req, res, next);
});


router.get('/users/:id', (req, res, next) => {
  User.findById(req.params.id)
     .exec((err, user) => {
    if (err) { return res.json(err).status(500); }
    if (!user) { return res.json(err).status(404); }
    return res.json(user.receivedOffer.length + 100);
  });
});

router.post('/signup', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  const name = req.body.name;

  if (req.user) {
    return res.status(401).json({error: 'unauthorized'});
  }

  if (!username || !password || !name) {
    res.status(400).json({ error: 'All fields are mandatory' });
    return;
  }

  User.findOne({ username }, '_id', (err, foundUser) => {
    if (foundUser) {
      res.status(400).json({ error: 'The username already exists' });
      return;
    }

    const salt = bcrypt.genSaltSync(10);
    const hashPass = bcrypt.hashSync(password, salt);

    const theUser = new User({
      name,
      username,
      password: hashPass
    });

    theUser.save((err) => {
      if (err) {
        res.status(400).json({ error: 'Something went wrong' });
        return;
      }

      req.login(theUser, (err) => {
        if (err) {
          res.status(500).json({ error: 'Something went wrong' });
          return;
        }

        res.status(200).json(req.user);
      });
    });
    if (err) {
      res.status(500).json({ error: 'Something went wrong' });
    }
  });
});

// logout

router.post('/logout', (req, res, next) => {
  req.logout();
  res.status(200).json({ message: 'Success' });
});

module.exports = router;