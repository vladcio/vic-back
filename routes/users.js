const nodemailer = require('nodemailer');
const express = require('express');
const router = express.Router();
const User = require('../models/user');
const email = require('emailjs/email');
const dotenv = require('dotenv').config()
const emailPass = process.env.EMAIL_TOKEN;
const emailUser = process.env.EMAIL_USER;
const emailHost = process.env.EMAIL_HOST;
const emailPort = process.env.EMAIL_PORT;
const emailFrom = process.env.EMAIL_FROM;
const emailSubject = process.env.EMAIL_SUBJECT;
const emailTo = process.env.EMAIL_TO;

const transporter = nodemailer.createTransport({
  host: emailHost,
  port: emailPort,
  auth: {
    user: emailUser, // your domain email address
    pass: emailPass, // your password
    tls:{
      rejectUnauthorized:false
    }
  }
});
/* GET users listing. */
router.get('/users', (req, res, next) => {
  User.find({})
    .then((users) => res.json(users))
    .catch(next);
});


router.post('/send-contact-form', (req, res, next) => {
  const userId = req.body.id;
  const date = new Date();
  const dateFormated = date.toLocaleString('en-ES');
  const receivedMessageData = {
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phoneNumber,
    reason: req.body.reason,
    message: req.body.messageContent,
    createdAt: dateFormated,
    unreaded: true,
    requestStatus: 'Initialized',
  };

  const mailOptions = {
    from: emailFrom,
    to: receivedMessageData.email,
    bcc: emailTo,
    subject: emailSubject + ' ' + receivedMessageData.name,
    text: 'Your request have been sent',
    html: 'Your request have been sent:' + '<br>' + 
    '<br>' +
    'Content: ' + receivedMessageData.message + '<br>' +
    '<br>' +
    'Created At: ' + dateFormated,
  };

  User.findByIdAndUpdate(userId, { $push: { receivedMessage: receivedMessageData } })
    .then(() => {
      return transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      })
    })
    .then((response) => {
      return res.json(response);
      })
    .catch(err => {
      return next(err);
    });
});

// router.post('/send-email', (req, res, next) => {

//   const transporter = nodemailer.createTransport({
//     host: emailHost,
//     port: emailPort,
//     auth: {
//       user: emailUser, // your domain email address
//       pass: emailPass, // your password
//       tls:{
//         rejectUnauthorized:false
//       }
//     }
//   });

//   const mailOptions = {
//     from: emailFrom,
//     to: emailTo,
//     subject: emailSubject,
//     text: 'That was easy!',
//   };

//   transporter.verify(function(error, success) {
//     if (error) {
//          console.log(error);
//     } else {
//          console.log('Server is ready to take our messages');
//     }
//  });

//   transporter.sendMail(mailOptions, function(error, info){
//     if (error) {
//       console.log(error);
//     } else {
//       console.log('Email sent: ' + info.response);
//     }
//   })
// });

router.post('/send-offer-form', (req, res, next) => {
  const userId = req.body.id;
  const date = new Date();
  const dateFormated = date.toLocaleString('en-ES');
  const receivedOfferData = {
    offerId: req.body.offerId,
    firstName: req.body.firstName,
    surName: req.body.surName,
    email: req.body.email,
    phone: req.body.phone,
    companyName: req.body.companyName,
    address: req.body.address,
    city: req.body.city,
    zipCode: req.body.zipCode,
    country: req.body.country,
    profession: req.body.profession,
    otherProfession: req.body.otherProfession,
    application: req.body.application,
    otherApplication: req.body.otherApplication,
    categoryType: req.body.categoryType,
    modelId: req.body.modelId,
    width: req.body.width,
    height: req.body.height,
    projectName: req.body.projectName,
    messageContent: req.body.messageContent,
    createdAt: dateFormated,
    unreaded: true,
    requestStatus: 'Initialized'
  };


  const mailOptions = {
    from: emailFrom,
    to: receivedOfferData.email,
    bcc: emailTo,
    subject: emailSubject + receivedOfferData.offerId.toString() + ' ' + receivedOfferData.firstName + ' ' + receivedOfferData.surName + ' ' + receivedOfferData.projectName,
    text: 
    'First Name: ' + receivedOfferData.firstName +
    'Surname: ' + receivedOfferData.surName +
    'Email: ' + receivedOfferData.email +
    'Phone: ' + receivedOfferData.phone +
    'Company Name: ' + receivedOfferData.companyName +
    'Address: ' + receivedOfferData.address +
    'City: ' + receivedOfferData.city +
    'Zip Code: ' + receivedOfferData.zipCode +
    'Country: ' + receivedOfferData.country +
    'Profession: ' + receivedOfferData.profession +
    'Other Profession: ' + receivedOfferData.otherProfession +
    'Application: ' + receivedOfferData.application +
    'Other Application: ' + receivedOfferData.otherApplication +
    'Category Type: ' + receivedOfferData.categoryType +
    'Model Id: ' + receivedOfferData.modelId +
    'Width: ' + receivedOfferData.width +
    'Height: ' + receivedOfferData.height +
    'Project Name: ' + receivedOfferData.projectName +
    'Message Content: ' + receivedOfferData.messageContent +
    'Processing Status: ' + receivedOfferData.requestStatus + 
    'Created At: ' + dateFormated,
    html:  
    'Thank you for your enquiry, your id is ' + receivedOfferData.offerId.toString() + '<br>' +
    'Content: ' + '<br>' +
    'First Name: ' + receivedOfferData.firstName + '<br>' +
    'Surname: ' + receivedOfferData.surName + '<br>' +
    'Email: ' + receivedOfferData.email + '<br>' +
    'Phone: ' + receivedOfferData.phone + '<br>' +
    'Company Name: ' + receivedOfferData.companyName + '<br>' +
    'Address: ' + receivedOfferData.address + '<br>' +
    'City: ' + receivedOfferData.city + '<br>' +
    'ZipCode: ' + receivedOfferData.zipCode + '<br>' +
    'Country: ' + receivedOfferData.country + '<br>' +
    'Profession: ' + receivedOfferData.profession + '<br>' +
    'Other Profession: ' + receivedOfferData.otherProfession + '<br>' +
    'Application: ' + receivedOfferData.application + '<br>' +
    'Other Application: ' + receivedOfferData.otherApplication + '<br>' +
    'Category Type: ' + receivedOfferData.categoryType + '<br>' +
    'Model Id: ' + receivedOfferData.modelId + '<br>' +
    'Width: ' + receivedOfferData.width + ' cm' + '<br>' +
    'Height: ' + receivedOfferData.height + ' cm' + '<br>' +
    'Project Name: ' + receivedOfferData.projectName + '<br>' +
    'Message Content: ' + receivedOfferData.messageContent + '<br>' +
    'Processing Status: ' + receivedOfferData.requestStatus + 
    '<br>' +
    'Created At: ' + dateFormated,
  };

//   transporter.verify(function(error, success) {
//     if (error) {
//          console.log(error);
//     } else {
//          console.log('Server is ready to take our messages');
//     }
//  });

User.findByIdAndUpdate(userId, { $push: { receivedOffer: receivedOfferData } })
  .then(() => {
    return transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    })
  })
  .then((response) => {
    return res.json(response);
  })
  .catch(err => {
    return next(err);
  });
});

module.exports = router;
