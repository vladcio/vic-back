require('dotenv').config();

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

const mongoose = require('mongoose');
const session = require('express-session');
const passport = require('passport');
const MongoStore = require('connect-mongo')(session);
const multer = require('multer');
const upload = multer({ dest: './public/uploads/' });

const passportSetup = require('./misc/passport');
const index = require('./routes/index');
const auth = require('./routes/auth');
const categories = require('./routes/categories');
const pictures = require('./routes/pictures');
// const requests = require('./routes/requests');
const users = require('./routes/users');


mongoose.Promise = Promise;
  mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  keepAlive: true,
  reconnectTries: Number.MAX_VALUE
})

passportSetup(passport);

// view engine setup
app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, 'public')));

app.use(cors({
  credentials: true,
  origin: [process.env.CLIENT_URL]
}));

app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});

app.use(session({
  store: new MongoStore({
    mongooseConnection: mongoose.connection,
    ttl: 24 * 60 * 60 // 1 day
  }),
  secret: 'some-string',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 24 * 60 * 60 * 1000
  }
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', index);
app.use('/auth', auth);
app.use('/categories', categories);
app.use('/pictures', pictures);
// app.use('/', requests);
app.use('/users', users);


// catch 404 and forward to error handler
app.use((req, res, next) => {
  res.status(404).json({error: 'not found'});
});

// error handler
app.use((err, req, res, next) => {
  // always log the error
  console.error('ERROR', req.method, req.path, err);

  // only render if the error ocurred before sending the response
  if (!res.headersSent) {
    res.status(500).json({error: 'unexpected'});
  }
});

module.exports = app;
