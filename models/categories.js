'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const itemSchema = new Schema ({
  name: String,
  categoryType: String,
  picPath: String,
  description: String,
  idReference: String,
});

const Item = mongoose.model('Item', itemSchema);

module.exports = Item;