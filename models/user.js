'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema ({
  username: String,
  password: String,
  name: String,
  receivedMessage: [{
    name: String,
    email: String,
    phone: String,
    message: String,
    createdAt: String,
    unreaded: Boolean,
    requestStatus: String,
  }],
  receivedOffer: [{
    firstName: String,
    surName: String,
    email: String,
    phone: String,
    companyName: String,
    address: String,
    city: String,
    zipCode: String,
    country: String,
    profession: String,
    otherProfession: String,
    application: String,
    otherApplication:String,
    categoryType: String,
    modelId: Number,
    width: Number,
    height: Number,
    projectName: String,
    messageContent: String,
    createdAt: String,
    unreaded: Boolean,
    offerId: Number,
    requestStatus: String,
    internalComments: String,
  }],
});

const User = mongoose.model('User', userSchema);

module.exports = User;