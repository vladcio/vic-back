'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pictureSchema = new Schema ({
  name: String,
  categoryType: String,
  modelType: String,
  picPath: String,
  description: String,
  idReference: String,
});

const Picture = mongoose.model('Picture', pictureSchema);

module.exports = Picture;